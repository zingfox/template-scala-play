package persists.postgres

import javax.inject.{Inject, Singleton}

import anorm.SqlParser._
import anorm._
import entities.TemplateEntity
import org.joda.time.DateTime
import persists.TemplatePersist
import play.api.db.Database
import utils.DateTimeConverter._

import scala.language.postfixOps

@Singleton
class TemplatePostgres @Inject()(db: Database)
  extends BasePostgres
    with TemplatePersist {

  override def findAll: List[TemplateEntity] =
    db.withConnection { implicit connection =>
      SQL(FIND_ALL)
        .as(templateEntityParser *)
    }

  override def find(id: String): Option[TemplateEntity] =
    db.withConnection { implicit connection =>
      SQL(FIND).onParams(
        id
      ).as(templateEntityParser singleOpt)
    }

  override def findByName(name: String): List[TemplateEntity] =
    db.withConnection { implicit connection =>
      SQL(FIND_BY_NAME).onParams(
        name
      ).as(templateEntityParser *)
    }

  override def insert(templateEntity: TemplateEntity): Boolean =
    db.withConnection { implicit connection =>
      SQL(INSERT).onParams(
        templateEntity.id
        , templateEntity.name
        , templateEntity.version
        , templateEntity.createdAt.toTimeStamp
      ).executeStatement()
    }

  override def update(id: String, name: String): Boolean =
    db.withConnection { implicit connection =>
      SQL(UPDATE).onParams(
        name
        , id
      ).executeStatement()
    }

  override def delete(id: String): Boolean =
    db.withConnection { implicit connection =>
      SQL(DELETE).onParams(
        id
      ).executeStatement()
    }


  private lazy val templateEntityParser: RowParser[TemplateEntity] = (
    str("id")
      ~str("name")
      ~int("version").?.map(_ getOrElse 1)
      ~date("created_at").map(new DateTime(_)))
    .map(TemplateEntity.rowParse)

  private lazy val FIND_ALL: String =
    """
      | SELECT * FROM templates
    """.stripMargin

  private lazy val FIND: String =
    """
      | SELECT * FROM templates
      | WHERE id = {id}
    """.stripMargin

  private lazy val FIND_BY_NAME: String =
    """
      | SELECT * FROM templates
      | WHERE name = {name}
    """.stripMargin

  private lazy val INSERT: String =
    """
      | INSERT INTO templates(
      |   id
      |   , name
      |   , version
      |   , created_at)
      | VALUES (
      |   {id}
      |   , {name}
      |   , {version}
      |   , {created_at})
    """.stripMargin

  private lazy val UPDATE: String =
    """
      | UPDATE templates
      | SET
      |   name = {name}
      | WHERE id = {id}
    """.stripMargin

  private lazy val DELETE: String =
    """
      | DELETE FROM templates
      | WHERE id = {id}
    """.stripMargin

}
