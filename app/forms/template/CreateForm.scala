package forms.template

import play.api.data.Forms._
import play.api.data.Form

/**
  * Created by ZingFox on 5/30/2017 AD.
  */
case class CreateForm(name: String
                      , version: Int)

object CreateForm {
  val form: Form[CreateForm] = Form(mapping(
    "name" -> nonEmptyText
    , "version" -> number
  )(CreateForm.apply)(CreateForm.unapply))
}
