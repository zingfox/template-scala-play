package definitions

import java.io.{PrintWriter, StringWriter}

import com.typesafe.scalalogging.LazyLogging
import play.api.data.Form
import definitions.constants.Template._

object AppException
  extends LazyLogging {

  type TemplateAlreadyExistException              = TemplateAlreadyExist
  type TemplateNotFoundException                  = TemplateNotFound
  type TemplateNotCreatedException                = TemplateNotCreated
  type TemplateNotUpdatedException                = TemplateNotUpdated
  type TemplateNotDeletedException                = TemplateNotDeleted
  type TemplateInvalidException                   = TemplateInvalid
  type TemplateNameInvalidException               = TemplateNameInvalid
  type TemplateVersionInvalidException            = TemplateVersionInvalid

  type UnauthorizedException                      = Unauthorized
  type UnauthorizedTokenMissingException          = UnauthorizedTokenMissing

  type FormException                              = FormError
  type PersistException                           = PersistError
  type UnexpectedException                        = UnexpectedError


  class TemplateAlreadyExist(name: String, version: Int)
    extends Exception(s"template $name version $version already exist.")

  class TemplateNotFound
    extends Exception("template not found.")

  class TemplateNotCreated
    extends Exception("template not created.")

  class TemplateNotUpdated
    extends Exception("template not updated.")

  class TemplateNotDeleted
    extends Exception("template not deleted.")

  class TemplateInvalid(message: String = "")
    extends Exception("template is invalid." + description(message))

  class TemplateNameInvalid
    extends TemplateInvalid(s"name cannot be longer than $MAX_NAME_LENGTH characters.")

  class TemplateVersionInvalid
    extends TemplateInvalid(s"version cannot be more than $MAX_VERSION.")

  class Unauthorized(message: String)
    extends Exception("unauthorized." + description(message))

  class UnauthorizedTokenMissing
    extends Unauthorized("token is missing")

  class FormError(message: String)
    extends Exception("input error." + description(message)) {

    def this(form: Form[_]) =
      this(
        form.errors.map { error =>
          error.key + " " + error.message.replace("error.", "")
        }.mkString(", ")
      )
  }

  class PersistError(message: String = "")
    extends Exception("persist error." + description(message))

  class UnexpectedError(other: Throwable)
    extends Exception("something went wrong. please try again.") {

    val stringWriter = new StringWriter
    other.printStackTrace(new PrintWriter(stringWriter))
    logger.error(stringWriter.toString)
  }

  private def description(message: String): String =
    message.isEmpty match {
      case true => ""
      case false => " " + message
    }

}
