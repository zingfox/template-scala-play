package spec

import org.scalatest.{BeforeAndAfterAll, Suite}
import play.api.db.Database
import utils.Json

trait DatabaseSpecHelper
  extends BeforeAndAfterAll
    with Json { this: Suite =>

  val db: Database

  protected def executeStatement(sql: String): Boolean = db.withConnection { implicit connection =>
    connection.prepareStatement(sql).execute()
  }

  protected def clearTables(names: String*): Seq[Boolean] = {
    val clearDatabasesSql = names.map { name =>
      s"DELETE FROM $name"
    }

    clearDatabasesSql.map(executeStatement)
  }

  override def afterAll(): Unit = {
    db.shutdown()
  }

}
