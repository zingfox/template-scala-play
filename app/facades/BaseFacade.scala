package facades

import definitions.AppException.UnexpectedException
import utils.Validation.Guard

import scala.util.{Failure, Try}

abstract class BaseFacade {

  protected def TryWith[T](guards: Guard*)(action: => T): Try[T] = Try {
    guards foreach (_.execute())
    action
  } recoverWith {
    case e: Exception if guards exists (_.isExpecting(e)) => Failure(e)
    case other => Failure(new UnexpectedException(other))
  }

}
