template_project - Backend
===
Server-Side template_project Application powered by Play Framework 2.5.x with Scala 2.11.x

Installation
---

#### Minimum
- JDK 8
- Scala
- sbt
- PostgreSQL
- IntelliJ IDEA

#### Recommended for OSX
- Postico
- Homebrew

#### Instruction for OSX
- install Homebrew
```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

- install JDK
```bash
brew cask install java
```

- install Scala and sbt
```bash
brew install -y scala sbt 
```

- install postgreSQL
```bash
brew install -y postgresql
```

- install other recommended applications
```bash
brew cask install intellij-idea-ce postico
```

TL;DR
```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew cask install java intellij-idea-ce postico
brew install -y scala sbt typesafe-activator postgresql
```

#### Instruction for Linux (Ubuntu or Debian-based distributions)
- add sbt repository
```bash
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt-get update
```

- install JRE/JDK
```bash
sudo apt-get install default-jre default-jdk
```

- install Scala and sbt
```bash
sudo apt-get install sbt
```

- install postgreSQL
```bash
sudo apt-get install postgresql
```

- install IntelliJ IDEA ([Installation](https://www.jetbrains.com/help/idea/installing-and-launching.html))

TL;DR
```bash
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt-get update
sudo apt-get install default-jre default-jdk sbt postgresql
```

Getting Started
---
#### Start Server in Local Development Environment
- setup local development database
```bash
./setup_localdb.sh development
```

- start local server 
```bash
sbt run
```

- access the server by hostname : 
```bash
http://localhost:9000
```

#### Test
- setup local test database
```bash
./setup_localdb.sh test
```

- run all test suites
```bash
sbt test
```

#### API references
- to be updated

```
https://to-be-updated.com
```

License
---
@ 2017 ALL RIGHT RESERVED, CODE APP.,CO.LTD
