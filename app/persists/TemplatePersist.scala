package persists

import entities.TemplateEntity

trait TemplatePersist {

  def findAll: List[TemplateEntity]

  def find(id: String): Option[TemplateEntity]

  def findByName(name: String): List[TemplateEntity]

  def insert(templateEntity: TemplateEntity): Boolean

  def update(id: String, name: String): Boolean

  def delete(id: String): Boolean

}
