package utils

object Validation {

  type Guard                 = ExceptionGuard
  val Guard                  = ExceptionGuard

  val Validate               = ExceptionValidate


  protected class ExceptionGuard(condition: => Boolean, exception: => Exception) {

    def execute(): Unit =
      if (condition) throw exception

    def expectedExceptionType: Class[_ <: Exception] =
      exception.getClass

    def isExpecting(exception: Exception): Boolean =
      expectedExceptionType.isAssignableFrom(exception.getClass)

  }

  protected object ExceptionGuard {

    def apply(condition: => Boolean, exception: => Exception): ExceptionGuard =
      new ExceptionGuard(condition, exception)

    def apply(exception: => Exception): ExceptionGuard =
      new ExceptionGuard(false, exception)

  }

  protected object ExceptionValidate {

    def apply(condition: => Boolean, exception: => Exception): Unit =
      new ExceptionGuard(condition, exception).execute()

  }

}
