package facades

import javax.inject.{Inject, Singleton}

import definitions.AppException._
import entities.TemplateEntity
import forms.template._
import models.Template
import org.joda.time.DateTime
import persists.TemplatePersist
import services.UuidService
import utils.Validation.Guard

import scala.util.Try

@Singleton
class TemplateFacade @Inject()(templatePersist: TemplatePersist
                               , uuidService: UuidService)
  extends BaseFacade {

  def findAll: Try[List[Template]] = TryWith() {
    templatePersist.findAll map Template.apply
  }

  def find(id: String): Try[Template] = TryWith(
    Guard(mayBeTemplateEntity(id).isEmpty, new TemplateNotFoundException)
  ) {
    Template(mayBeTemplateEntity(id).get)
  }

  def create(form: CreateForm)
            (implicit token: String): Try[Template] = TryWith(
    Guard(new TemplateInvalidException)
    , Guard(isAlreadyExist(form.name, form.version), new TemplateAlreadyExistException(form.name, form.version))
  ) {
    val template = Template(
      uuidService.getId.toString
      , form.name
      , form.version
      , DateTime.now
    )

    templatePersist.insert(TemplateEntity(template)) match {
      case true => template
      case false => throw new TemplateNotCreatedException
    }
  }

  def update(id: String, form: UpdateForm)
            (implicit token: String): Try[Template] = {
    lazy val existingTemplate = Template(mayBeTemplateEntity(id).get)
    lazy val version = existingTemplate.version

    TryWith(
      Guard(new TemplateInvalidException)
      , Guard(mayBeTemplateEntity(id).isEmpty, new TemplateNotFoundException)
      , Guard(isAlreadyExist(form.name, version), new TemplateAlreadyExistException(form.name, version))
    ) {
      val template = existingTemplate.copy(name = form.name)

      templatePersist.update(template.id, template.name) match {
        case true => template
        case false => throw new TemplateNotUpdatedException
      }
    }
  }

  def delete(id: String)
            (implicit token: String): Try[Template] = TryWith(
    Guard(mayBeTemplateEntity(id).isEmpty, new TemplateNotFoundException)
  ) {
    val template = Template(mayBeTemplateEntity(id).get)

    templatePersist.delete(id) match {
      case true => template
      case false => throw new TemplateNotDeletedException
    }
  }


  private def mayBeTemplateEntity(id: String): Option[TemplateEntity] =
    templatePersist.find(id)

  private def isAlreadyExist(name: String, version: Int): Boolean =
    templatePersist.findByName(name).map(_.version).contains(version)

}
