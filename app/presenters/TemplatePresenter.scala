package presenters

import models.Template
import utils.DateTimeConverter._

case class TemplatePresenter(name: String
                             , createdAt: Long)

object TemplatePresenter {

  def apply(template: Template): TemplatePresenter =
    TemplatePresenter(
      s"${template.name} (${template.version})"
      , template.createdAt.toUnixTime
    )

  def apply(templates: List[Template]): List[TemplatePresenter] =
    templates map TemplatePresenter.apply

}