# --- !Ups

CREATE TABLE templates
(
    id                      VARCHAR(64) NOT NULL UNIQUE,
    name                    VARCHAR(256),
    created_at              TIMESTAMP WITH TIME ZONE NOT NULL,
    CONSTRAINT              templates_id PRIMARY KEY (id)
);

# ---!Downs

DROP TABLE templates;