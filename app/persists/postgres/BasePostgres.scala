package persists.postgres

import java.sql.Connection

import anorm.{Row, SimpleSql}
import definitions.AppException.PersistException
import utils.Json

import scala.language.implicitConversions

abstract class BasePostgres
  extends Json {

  protected def parse[T <: Object: Manifest](string: String): T =
    Json.toObject[T](string) match {
      case Some(obj) if obj != null => obj
      case _ => throw new PersistException(s"""cannot parse "$string" to ${manifest[T]}""")
    }

  implicit class SqlStatement(sqlStatement: SimpleSql[Row])
                             (implicit connection: Connection) {
    def executeStatement(expectedChangedRow: Int = 1): Boolean =
      expectedChangedRow == 1 match {
        case true => updateSingleRow()
        case false => updateBatch(expectedChangedRow)
      }

    private def updateSingleRow(): Boolean =
      sqlStatement.executeUpdate == 1

    private def updateBatch(expectedChangedRow: Int): Boolean = {
      connection.setAutoCommit(false)

      sqlStatement.executeUpdate == expectedChangedRow match {
        case true => connection.commit(); true
        case false if sqlStatement.executeUpdate == 0 => false
        case false => connection.rollback(); false
      }
    }
  }

}
