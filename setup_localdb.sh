#!/bin/bash

if [[ "$#" -ne 1 ]]; then
  echo "Setup local postgresql database"
  echo ""
  echo "usage : $0 ENVIRONMENT (development, test)"
  echo "ex : $0 development"
else
  echo "Setting up local postgresql database for $1 environment"
  echo ""
  createuser -s template_project
  dropdb --if-exists "template_project_$1"
  createdb "template_project_$1" -O template_project -E utf8
  echo ""
  echo "DONE"
fi
