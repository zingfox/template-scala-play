package controllers

import play.api.mvc.{Result, Results}
import presenters.ResponsePresenter
import utils.Json

import scala.util.{Failure, Try}

trait TryResults
  extends Results
    with Json {

  protected implicit class PresenterTry[+T](result: Try[T]) {
    def toResult: Result = {
      val response = ResponsePresenter(result)

      resultApplicationJson(response)
    }

    def toResult(message: String): Result = {
      val response = ResponsePresenter(message)

      resultApplicationJson(response)
    }

    def toResultWithMessage(message: String): Result = {
      val response = ResponsePresenter(result, message)

      resultApplicationJson(response)
    }

    private def resultApplicationJson(response: ResponsePresenter[_]): Result = {
      result.isSuccess match {
        case true => Ok(response.toJson) as APPLICATION_JSON
        case false => BadRequest(response.toJson) as APPLICATION_JSON
      }
    }
  }

  protected implicit class FailureTry(failure: Failure[Exception]) {
    def unauthorized: Result = Unauthorized(ResponsePresenter(failure).toJson) as APPLICATION_JSON
  }

  private lazy val APPLICATION_JSON: String = "application/json"

}
