package controllers.api

import controllers.CustomAction
import definitions.AppException.FormException
import play.api.data.Form
import play.api.mvc.{Controller, Request}

abstract class ApiController
  extends Controller
    with CustomAction {

  protected def requestForm[T](form: Form[T])
                              (implicit request: Request[_]): T =
    form.bindFromRequest.fold(
      formError => throw new FormException(formError)
      , form => form
    )

}
