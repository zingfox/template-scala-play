package utils

import play.api.http.HttpVerbs._

import scala.language.postfixOps
import scalaj.http.{Http, HttpRequest, HttpResponse, StringBodyConnectFunc}

object HttpClient
  extends Json {

  def request(method: String
              , path: String
              , body: Object = null)
             (implicit headers: List[(String, String)] = List()): HttpResponse[String] = {
    val http = Http(path)
      .method(method)
      .headers(DEFAULT_HEADERS ::: headers)

    method match {
      case GET =>
        http.asString
      case _ =>
        http.body(body)
          .asString
    }
  }


  private implicit class HttpRequestExtension(httpRequest: HttpRequest) {
    def body(body: Object): HttpRequest =
      httpRequest.copy(connectFunc = StringBodyConnectFunc(body.toJson))
  }

  private val DEFAULT_HEADERS: List[(String, String)] = List(("Content-Type", "application/json"))

}

