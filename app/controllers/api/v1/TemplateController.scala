package controllers.api.v1

import javax.inject.{Inject, Singleton}

import controllers.api.ApiController
import facades.TemplateFacade
import forms.template._
import presenters.TemplatePresenter

import scala.language.postfixOps

@Singleton
class TemplateController @Inject()(templateFacade: TemplateFacade)
  extends ApiController {

  def list = Action {
    templateFacade.findAll map TemplatePresenter.apply toResult
  }

  def get(id: String) = Action {
    templateFacade.find(id) map TemplatePresenter.apply toResult
  }

  def post = AuthenticatedAction { implicit request =>
    implicit val token = request.token

    val form = requestForm(CreateForm.form)

    templateFacade.create(form) toResult (message = "template created.")
  }

  def put(id: String) = AuthenticatedAction { implicit request =>
    implicit val token = request.token

    val form = requestForm(UpdateForm.form)

    templateFacade.update(id, form) toResult (message = "template updated.")
  }

  def delete(id: String) = AuthenticatedAction { request =>
    implicit val token = request.token

    templateFacade.delete(id) toResult (message = "template deleted.")
  }

}
