package spec

import com.typesafe.config.ConfigFactory
import org.scalatest._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.db.Database
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.{Application, Configuration}

abstract class BaseSpec
  extends WordSpec
    with Matchers
    with CustomMatcher
    with OptionValues
    with TryValues
    with GuiceOneAppPerSuite
    with PrivateMethodTester
    with ControllerSpecHelper
    with DatabaseSpecHelper {

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .loadConfig(Configuration(ConfigFactory.load("test")))
    .build()

  override val db: Database = app.injector.instanceOf[Database]

  implicit val token: String = "1234567890"

}
