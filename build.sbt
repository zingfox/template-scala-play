name := """template_project"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)

scalaVersion := "2.11.8"
scalacOptions ++= Seq("-deprecation", "-feature")

libraryDependencies ++= Seq(
  jdbc
    exclude("com.h2database", "h2")
    exclude("com.jolbox", "bonecp")
  , cache, ws, filters, evolutions
  , "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.7.2"
  , "com.twitter" %% "util-core" % "6.29.0"
  , "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0"
  , "commons-codec" % "commons-codec" % "1.10"
  , "net.codingwell" %% "scala-guice" % "4.0.0"
  , "org.mindrot" % "jbcrypt" % "0.3m"
  , "org.postgresql" % "postgresql" % "9.4.1208"
  , "com.typesafe.play" %% "anorm" % "2.5.0"
  , "org.scalaj" %% "scalaj-http" % "2.3.0"
)

libraryDependencies ++= Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "2.0.0" % Test
)

fork in run := false

parallelExecution in Test := true
