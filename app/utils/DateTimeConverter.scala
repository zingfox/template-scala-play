package utils

import java.sql.Timestamp

import org.joda.time.DateTime

object DateTimeConverter {

  implicit class JodaDateTime(dateTime: DateTime) {
    def toUnixTime: Long = dateTime.getMillis / 1000

    def toTimeStamp: Timestamp = new Timestamp(dateTime.getMillis)
  }

}
