package models

import definitions.AppException.{TemplateNameInvalidException, TemplateVersionInvalidException}
import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import definitions.constants.Template._
import spec.BaseSpec

@RunWith(classOf[JUnitRunner])
class TemplateSpec
  extends BaseSpec {

  "Template" should {
    "return Template if success" in {
      val id = "1"
      val name = "Template #1"
      val version = 1
      val createdAt = new DateTime

      val result = Template(
        id
        , name
        , version
        , createdAt
      )

      result shouldBe a [Template]
    }

    "throw TemplateNameInvalidException if template name is invalid" in {
      val id = "1"
      val name = "a" * (MAX_NAME_LENGTH + 1)
      val version = 1
      val createdAt = new DateTime

      lazy val result = Template(
        id
        , name
        , version
        , createdAt
      )

      the [TemplateNameInvalidException] thrownBy result
    }

    "throw TemplateVersionInvalidException if template version is invalid" in {
      val id = "1"
      val name = "Template #1"
      val version = MAX_VERSION + 1
      val createdAt = new DateTime

      lazy val result = Template(
        id
        , name
        , version
        , createdAt
      )

      the [TemplateVersionInvalidException] thrownBy result
    }
  }

}
