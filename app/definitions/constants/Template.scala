package definitions.constants

object Template {

  val MAX_NAME_LENGTH: Int = 256
  val MAX_VERSION: Int = 100

}
