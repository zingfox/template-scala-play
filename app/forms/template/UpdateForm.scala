package forms.template

import play.api.data.Forms._
import play.api.data.Form

/**
  * Created by ZingFox on 5/30/2017 AD.
  */
case class UpdateForm(name: String)

object UpdateForm {
  val form: Form[UpdateForm] = Form(mapping(
    "name" -> nonEmptyText
  )(UpdateForm.apply)(UpdateForm.unapply))
}
