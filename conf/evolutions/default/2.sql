# --- !Ups

ALTER TABLE templates ADD version INT;

# ---!Downs

ALTER TABLE templates DROP version;