package facades

import definitions.AppException.{TemplateAlreadyExistException, TemplateNotFoundException}
import forms.template._
import models.Template
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import spec.BaseSpec
import spec.data.TemplateData

@RunWith(classOf[JUnitRunner])
class TemplateFacadeSpec
  extends BaseSpec
    with TemplateData {

  "findAll" should {
    "return Success[List[Template]]" in {
      val result = templateFacade.findAll

      result.success.value shouldBe a [List[_]]
      all(result.success.value) shouldBe a [Template]
    }
  }

  "find(id)" should {
    "return Success[Template]" in {
      val id = "1"

      val result = templateFacade.find(id)

      result.success.value shouldBe a [Template]
    }

    "return Failure[TemplateNotFoundException] if template not found" in {
      val id = "99"

      val result = templateFacade.find(id)

      result.failure.exception shouldBe a [TemplateNotFoundException]
    }
  }

  "create(form)" should {
    "return Success[Template] if success" in {
      val name = "template 1"
      val version = 2
      val form = CreateForm(
        name
        , version
      )

      val result = templateFacade.create(form)

      result.success.value shouldBe a [Template]
    }

    "return Failure[TemplateAlreadyExistException] if template already exist" in {
      val name = "template 1"
      val version = 1
      val form = CreateForm(
        name
        , version
      )

      val result = templateFacade.create(form)

      result.failure.exception shouldBe a [TemplateAlreadyExistException]
    }
  }

  "update(id, name)" should {
    "return Success[Template] if success" in {
      val id = "1"
      val name = "template #1"
      val form = UpdateForm(
        name
      )

      val result = templateFacade.update(id, form)

      result.success.value shouldBe a [Template]
    }

    "return Failure[TemplateNotFoundException] if template not found" in {
      val id = "99"
      val name = "template #1"
      val form = UpdateForm(
        name
      )

      val result = templateFacade.update(id, form)

      result.failure.exception shouldBe a [TemplateNotFoundException]
    }

    "return Failure[TemplateAlreadyExistException] if template already exist" in {
      val id = "1"
      val name = "template 2"
      val form = UpdateForm(
        name
      )

      val result = templateFacade.update(id, form)

      result.failure.exception shouldBe a [TemplateAlreadyExistException]
    }
  }

  "delete(id)" should {
    "return Success[Template] if success" in {
      val id = "1"

      val result = templateFacade.delete(id)

      result.success.value shouldBe a [Template]
    }

    "return Failure[TemplateNotFoundException] if template not found" in {
      val id = "99"

      val result = templateFacade.delete(id)

      result.failure.exception shouldBe a [TemplateNotFoundException]
    }
  }


  private lazy val templateFacade = app.injector.instanceOf[TemplateFacade]

}
