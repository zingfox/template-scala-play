package persists

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import persists.postgres.TemplatePostgres

case class PersistModule()
  extends AbstractModule
    with ScalaModule {

  protected def configure() {
    bind[TemplatePersist].to[TemplatePostgres]
  }

}
