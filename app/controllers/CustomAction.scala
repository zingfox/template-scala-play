package controllers

import definitions.AppException.{FormException, UnauthorizedException, UnauthorizedTokenMissingException}
import play.api.mvc._

import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Failure

trait CustomAction
  extends TryResults {

  protected def Action = new ActionBuilder[Request] {
    override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = {
      try {
        block(request)
      } catch {
        case e: FormException => response(Failure(e).toResult)
      }
    }
  }

  protected def AuthenticatedAction = new ActionBuilder[AuthenticatedRequest] {
    override def invokeBlock[A](request: Request[A], block: (AuthenticatedRequest[A]) => Future[Result]): Future[Result] = {
      try {
        block(AuthenticatedRequest(request, getApiToken(request)))
      } catch {
        case e: UnauthorizedException => response(Failure(e).unauthorized)
        case e: FormException => response(Failure(e).toResult)
      }
    }
  }


  private def response(result: Result): Future[Result] =
    Future.successful(result)

  private def getApiToken[A](implicit request: Request[A]): String =
    getToken(API_TOKEN)

  private def getToken[A](key: String)
                         (implicit request: Request[A]): String =
    request.headers.get("X-Api-Token") match {
      case Some(token) => token
      case None => throw new UnauthorizedTokenMissingException
    }

  private val API_TOKEN: String = "X-Api-Token"


  case class AuthenticatedRequest[A](request: Request[A]
                                     , token: String)
    extends WrappedRequest[A](request)

}
