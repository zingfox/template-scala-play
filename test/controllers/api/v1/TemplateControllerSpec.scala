package controllers.api.v1

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import play.api.test.Helpers._
import spec.BaseSpec
import spec.data.TemplateData

@RunWith(classOf[JUnitRunner])
class TemplateControllerSpec
  extends BaseSpec
    with TemplateData {

  implicit val headers: List[(String, String)] = List("X-Api-Token" -> "1234567890")

  "GET /api/v1/templates" should {
    "return 200" in {
      val response = request(GET, "/api/v1/templates")

      response should haveStatus (200)
      response should beJsonParsable
    }
  }

  "GET /api/v1/templates/:id" should {
    "return 200 if success" in {
      val id = "1"

      val response = request(GET, s"/api/v1/templates/$id")

      response should haveStatus (200)
      response should beJsonParsable
    }

    "return 400 if fail" in {
      val id = "99"

      val response = request(GET, s"/api/v1/templates/$id")

      response should haveStatus (400)
      response should beJsonParsable
    }
  }

  "POST /api/v1/templates" should {
    "return 200 if success" in {
      val body = json(
        "name" -> "template 1"
        , "version" -> 2
      )

      val response = request(POST, "/api/v1/templates", body)

      response should haveStatus (200)
      response should beJsonParsable
    }

    "return 400 if fail" in {
      val response = request(POST, "/api/v1/templates")

      response should haveStatus (400)
      response should beJsonParsable
    }
  }

  "PUT /api/v1/templates/:id" should {
    "return 200 if success" in {
      val id = "1"
      val body = json(
        "name" -> "template #1"
      )

      val response = request(PUT, s"/api/v1/templates/$id", body)

      response should haveStatus (200)
      response should beJsonParsable
    }

    "return 400 if fail" in {
      val id = "1"

      val response = request(PUT, s"/api/v1/templates/$id")

      response should haveStatus (400)
      response should beJsonParsable
    }
  }

  "DELETE /api/v1/templates/:id" should {
    "return 200 if success" in {
      val id = "1"

      val response = request(DELETE, s"/api/v1/templates/$id")

      response should haveStatus (200)
      response should beJsonParsable
    }

    "return 400 if fail" in {
      val id = "99"

      val response = request(DELETE, s"/api/v1/templates/$id")

      response should haveStatus (400)
      response should beJsonParsable
    }
  }

}
