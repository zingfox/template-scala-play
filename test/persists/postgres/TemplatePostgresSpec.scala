package persists.postgres

import entities.TemplateEntity
import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import spec.BaseSpec
import spec.data.TemplateData

@RunWith(classOf[JUnitRunner])
class TemplatePostgresSpec
  extends BaseSpec
    with TemplateData {

  "findAll" should {
    "return List[TemplateEntity]" in {
      val result = templatePostgres.findAll

      result shouldBe a [List[_]]
      all(result) shouldBe a [TemplateEntity]
    }
  }

  "find(id)" should {
    "return Some[TemplateEntity] if found" in {
      val id = "1"

      val result = templatePostgres.find(id)

      result.value shouldBe a [TemplateEntity]
    }

    "return None if not found" in {
      val id = "99"

      val result = templatePostgres.find(id)

      result shouldBe None
    }
  }

  "findByName(name)" should {
    "return List[TemplateEntity]" in {
      val name = "template 1"

      val result = templatePostgres.findByName(name)

      result shouldBe a [List[_]]
      all(result) shouldBe a [TemplateEntity]
    }
  }

  "insert(templateEntity)" should {
    "return true if success" in {
      val templateEntity = TemplateEntity(
        "4"
        , "template 1"
        , 2
        , DateTime.now
      )

      val result = templatePostgres.insert(templateEntity)

      result shouldBe true
    }
  }

  "update(id, name)" should {
    "return true if success" in {
      val id = "1"
      val name = "template #1"

      val result = templatePostgres.update(id, name)

      result shouldBe true
    }
  }

  "delete(id)" should {
    "return true if success" in {
      val id = "1"

      val result = templatePostgres.delete(id)

      result shouldBe true
    }
  }


  private lazy val templatePostgres = app.injector.instanceOf[TemplatePostgres]

}
