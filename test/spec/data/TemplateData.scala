package spec.data

import org.scalatest.{BeforeAndAfterEach, Suite}
import spec.DatabaseSpecHelper

trait TemplateData
  extends DatabaseSpecHelper
    with BeforeAndAfterEach { this: Suite =>

  private lazy val INSERT_TEST_DATA_TEMPLATE: String =
    """
      | INSERT INTO templates(
      |   id
      |   , name
      |   , created_at)
      | VALUES(
      |   '1'
      |   , 'template 1'
      |   , NOW())
      | , (
      |   '2'
      |   , 'template 2'
      |   , NOW())
      | , (
      |   '3'
      |   , 'template 3'
      |   , NOW())
    """.stripMargin

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    clearTables("templates")
    executeStatement(INSERT_TEST_DATA_TEMPLATE)
  }

  override protected def afterEach(): Unit = {
    super.afterEach()
    clearTables("templates")
  }

}
