package entities

import anorm._
import models.Template
import org.joda.time.DateTime

case class TemplateEntity(id: String
                          , name: String
                          , version: Int
                          , createdAt: DateTime)

object TemplateEntity {

  def apply(template: Template): TemplateEntity =
    TemplateEntity(
      template.id
      , template.name
      , template.version
      , template.createdAt
    )

  def rowParse(row: ~[~[~[String, String], Int], DateTime]): TemplateEntity =
    row match { case id ~ name ~ version ~ createdAt =>
      TemplateEntity(
        id
        , name
        , version
        , createdAt
      )
    }

}


