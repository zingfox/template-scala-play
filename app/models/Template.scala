package models

import definitions.AppException.{TemplateNameInvalidException, TemplateVersionInvalidException}
import entities.TemplateEntity
import org.joda.time.DateTime
import definitions.constants.Template._
import utils.Validation.Validate

case class Template(id: String
                    , name: String
                    , version: Int
                    , createdAt: DateTime) {

  Validate(isNameTooLong, new TemplateNameInvalidException)
  Validate(isVersionLimitExceed, new TemplateVersionInvalidException)

  private def isNameTooLong: Boolean =
    name.length > MAX_NAME_LENGTH

  private def isVersionLimitExceed: Boolean =
    version > MAX_VERSION

}

object Template {

  def apply(templateEntity: TemplateEntity): Template =
    Template(
      templateEntity.id
      , templateEntity.name
      , templateEntity.version
      , templateEntity.createdAt
    )

}
