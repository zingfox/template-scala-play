package utils

import com.fasterxml.jackson.core.{JsonGenerator, JsonParser}
import com.fasterxml.jackson.databind.{DeserializationContext, JsonDeserializer, JsonSerializer, SerializerProvider}
import com.fasterxml.jackson.databind.module.SimpleModule
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

object JodaJacksonModule
  extends SimpleModule {

  private val format = "yyyy-MM-dd HH:mm:ss ZZ"
  private val dateTimeFormat = DateTimeFormat.forPattern(format)

  addDeserializer(classOf[DateTime], new JsonDeserializer[DateTime] {
    override def deserialize(jsonParser: JsonParser, context: DeserializationContext): DateTime =
      dateTimeFormat.parseDateTime(jsonParser.getText)
  })

  addSerializer(classOf[DateTime], new JsonSerializer[DateTime] {
    override def serialize(value: DateTime, jsonGenerator: JsonGenerator, serializers: SerializerProvider): Unit =
      jsonGenerator.writeString(dateTimeFormat.print(value))
  })
}
